//
//  GameScene.swift
//  SnakeAndLadders
//
//  Created by kevin travers on 2/3/16.
//  Copyright (c) 2016 Bunny Phantom. All rights reserved.
//

import SpriteKit
import AVFoundation
class GameScene: SKScene {
    var board = SKSpriteNode()
    //game state varibales
    var gamePlayInProgress:Bool = false
    var player1Turn:Bool = false
    var rolling:Bool = false
    var moveFinished:Bool = true
    var gameover:Bool = false
    //player postion
    var player1CurrentPosition:Int = 0
    var player2CurrentPosition:Int = 0
    //rolled dice
    var rolledDice:Int = 0
    
    //layer sprite node
    var player1:SKSpriteNode = SKSpriteNode()
    var player2:SKSpriteNode = SKSpriteNode()
    
    //player current postion label
    var player1CurrentPositionLabel:SKLabelNode = SKLabelNode()
    var player2CurrentPositionLabel:SKLabelNode = SKLabelNode()
    
    //status and tap
    var statusLabel:SKLabelNode = SKLabelNode()
    var tapLabel:SKLabelNode = SKLabelNode()
    
    //sound
    var diceSFX: SKAction = SKAction()
    var snakeSFX:SKAction = SKAction()
    var ladderSFX:SKAction = SKAction()
    var backgroundMusic:AVAudioPlayer = AVAudioPlayer()
    var gameOverMusic:AVAudioPlayer = AVAudioPlayer()
    override func didMoveToView(view: SKView) {
        board = childNodeWithName("board") as! SKSpriteNode
        player1 = childNodeWithName("player1") as! SKSpriteNode
        player2 = childNodeWithName("player2") as! SKSpriteNode
        player1CurrentPositionLabel = childNodeWithName("player1CurrentPositionLabel") as! SKLabelNode
        player2CurrentPositionLabel = childNodeWithName("player2CurrentPositionLabel") as! SKLabelNode
        
        player1CurrentPositionLabel.text = "0"
        player2CurrentPositionLabel.text = "0"
        statusLabel = childNodeWithName("statusLabel") as! SKLabelNode
        tapLabel = childNodeWithName("tapLabel") as! SKLabelNode
        
        statusLabel.text = "Touch screen"
        tapLabel.text = "Tap screen"
        
        let fadeOut = SKAction.fadeOutWithDuration(0.3)
        let fadeIN = SKAction.fadeInWithDuration(0.3)
        let sequence = SKAction.sequence([fadeIN,fadeOut])
        let repeatAction = SKAction.repeatActionForever(sequence)
        tapLabel.runAction(repeatAction)
        tapLabel.hidden = true
        setUpSound()
        backgroundMusic.play()
    }
    func setUpSound(){
        let url: NSURL = NSBundle.mainBundle().URLForResource("gameplaybackground", withExtension: "mp3")!
        do{
            backgroundMusic = try AVAudioPlayer(contentsOfURL: url)
        }catch _{
            print("Error loading the background music")
        }
        
        backgroundMusic.numberOfLoops = -1
        backgroundMusic.prepareToPlay()
        let urlGameOver:NSURL = NSBundle.mainBundle().URLForResource("GameOver", withExtension: "mp3")!
        do{
            gameOverMusic = try AVAudioPlayer(contentsOfURL: urlGameOver)
        }catch _{
            print("Error loading the game over music")
        }
        gameOverMusic.numberOfLoops = 1
        gameOverMusic.prepareToPlay()
        
        diceSFX = SKAction.playSoundFileNamed("dice.caf", waitForCompletion: false)
        ladderSFX = SKAction.playSoundFileNamed("ladder.caf", waitForCompletion: false)
        snakeSFX = SKAction.playSoundFileNamed("snake.caf", waitForCompletion: false)
        
    }
    //check if hit ladder or snake
    func checkHit(currentPosition currentPosition: Int)->Bool
    {
        var result:Bool = false
        switch(currentPosition) {
        case 7,20,25,28,36,42,51,56,59,62,69,71,83,86,91,94,99:
            result = true
        default:
            result = false
        }
        return result
    }
    //find new postion
    func snakesAndLadders(currentPosition currentPosition: Int)->Float{
        var result = -1
        switch(currentPosition) {
        case 7:
            result = 11
        case 20:
            result = 38
        case 25:
            result = 3
        case 28:
            result = 65
        case 36:
            result = 44
        case 42:
            result = 63
        case 51:
            result = 67
        case 56:
            result = 48
        case 59:
            result = 1
        case 62:
            result = 81
        case 69:
            result = 32
        case 71:
            result = 90
        case 83:
            result = 57
        case 86:
            result = 97
        case 91:
            result = 73
        case 94:
            result = 26
        case 99:
            result = 80
        default:
            break
        }
        return Float(result)
        
    }
    //move player to new postion 1= you 2 = pc
    func move(player player:Int, postion:Float){
        
        let row:Int = Int(ceil(postion/10.0))
        var col:Int = 0
        if row % 2 != 0{
            if postion>0{
                col = Int(postion) - (row - 1)*10
            }else{
                col = Int(postion)
            }
        }else{
            let colPos:Int = Int(postion) - ((row - 1)*10)
            switch colPos{
            case 1:
                col = 10
            case 2:
                col = 9
            case 3:
                col = 8
            case 4:
                col = 7
            case 5:
                col = 6
            case 6:
                col = 5
            case 7:
                col = 4
            case 8:
                col = 3
            case 9:
                col = 2
            case 10:
                col = 1
            default:
                break
            }
        }
        let colMinus:Float = Float(col - 1)
        let rowMinus:Float = Float(row - 1)
        let cellWidth:Float = Float(board.frame.width/10.0)
        let xOffset = board.position.x
        let yOffset = board.position.y
        let x:Float = Float(xOffset) + colMinus * cellWidth + cellWidth/2
        let y:Float = Float(yOffset) + rowMinus * cellWidth + cellWidth/2
        
        if player == 1{
            let moveTo = SKAction.moveTo(CGPointMake(CGFloat(x),CGFloat(y)), duration: 1.0)
            player1.runAction(moveTo, completion: { () -> Void in
                self.player1CurrentPositionLabel.text = "\(Int(postion))"
                if self.checkHit(currentPosition: self.player1CurrentPosition) == true{
                    switch (self.player1CurrentPosition){
                    case 7,20,28,36,42,51,62,71,86:
                        self.runAction(self.ladderSFX)
                    default:
                        self.runAction(self.snakeSFX)
                    }
                    self.move(player: 1, postion: self.snakesAndLadders(currentPosition: self.player1CurrentPosition))
                    self.player1CurrentPosition = Int(self.snakesAndLadders(currentPosition: self.player1CurrentPosition))
                    return
                }
                if self.player1CurrentPosition == 100{
                    self.gameIsOver(player: 1)
                    return
                }
                self.moveFinished = true
                self.player1Turn = false
                self.statusLabel.text = "Pc Turn"
            
                let dice0 = self.childNodeWithName("dice")
                dice0?.removeFromParent()
                
                let delay  = SKAction.waitForDuration(1.0)
                self.runAction(delay, completion: { () -> Void in
                    self.moveFinished = false
                    self.rollDice(player: 2)
                })
                
            })
            
        }else{
            let moveTo = SKAction.moveTo(CGPointMake(CGFloat(x),CGFloat(y)), duration: 1.0)
            player2.runAction(moveTo, completion: { () -> Void in
                self.player2CurrentPositionLabel.text = "\(Int(postion))"
                if self.checkHit(currentPosition: self.player2CurrentPosition) == true{
                    switch (self.player2CurrentPosition){
                    case 7,20,28,36,42,51,62,71,86:
                        self.runAction(self.ladderSFX)
                    default:
                        self.runAction(self.snakeSFX)
                    }
                    self.move(player: 2, postion: self.snakesAndLadders(currentPosition: self.player2CurrentPosition))
                    self.player2CurrentPosition = Int(self.snakesAndLadders(currentPosition: self.player2CurrentPosition))
                    return
                }
                if self.player2CurrentPosition == 100{
                    self.gameIsOver(player: 2)
                    return
                }
                self.moveFinished = true
                self.player1Turn = true
                self.statusLabel.text = "Your Turn"
                self.tapLabel.hidden = false
                
                let dice0 = self.childNodeWithName("dice")
                dice0?.removeFromParent()

            })
        }
    }
    func startGame(){
        let turn = arc4random() % 2 + 1
        
        switch turn{
        case 1:
            statusLabel.text = "Your Turn"
            player1Turn = true
            tapLabel.hidden = true
        case 2:
            statusLabel.text = "Pc Turn"
            player1Turn = false
            moveFinished = false
            rollDice(player: 2)
        default:
            break
        }
        gamePlayInProgress = true
        gameover = false
    }
    func restart(){
        let scene = TitleScene(fileNamed: "TitleScene")
        self.view?.presentScene(scene)
    }
    func rollDice(player player:Int){
        if gameover == false{
            rolling = true
            self.enumerateChildNodesWithName("dice", usingBlock: { (diceNode: SKNode, stop:UnsafeMutablePointer<ObjCBool>) -> Void in
                diceNode.removeFromParent()
            })
            var diceImages = [SKTexture]()
            for var i = 0; i < 10; i++ {
                let imageNum = arc4random() % 6 + 1
                let imageName = "dice\(imageNum)"
                let diceRolled = SKTexture(imageNamed: "\(imageName)")
                diceImages.append(diceRolled)
                rolledDice = Int(imageNum)
                
            }
            let dice = SKSpriteNode(imageNamed: "dice1")
            if self.player1Turn == true{
                dice.position = CGPointMake(70, 190)
            }else{
                dice.position = CGPointMake(595, 190)
            }
            dice.name = "dice"
            self.addChild(dice)
            let diceAnimation = SKAction.animateWithTextures(diceImages, timePerFrame: 0.2)
            self.runAction(diceSFX)
            dice.runAction(diceAnimation, completion: { () -> Void in
                if self.player1Turn == true{
                    if (self.player1CurrentPosition + self.rolledDice) <= 100{
                        let pos = Float(self.player1CurrentPosition + self.rolledDice)
                        self.move(player: 1, postion: pos)
                        self.player1CurrentPosition += self.rolledDice
                    }else{
                        self.moveFinished = true
                        self.player1Turn = false
                        self.statusLabel.text = "Pc Turn"
                        
                        let delay = SKAction.waitForDuration(1.0)
                        self.runAction(delay, completion: { () -> Void in
                            self.moveFinished = false
                            self.rollDice(player: 2)
                        })
                    }
                }else{
                    if (self.player2CurrentPosition + self.rolledDice) <= 100{
                        let pos = Float(self.player2CurrentPosition + self.rolledDice)
                        self.move(player: 2, postion: pos)
                        self.player2CurrentPosition += self.rolledDice
                    }else{
                        self.moveFinished = true
                        self.player1Turn = true
                        self.statusLabel.text = "Your Turn"
                        
                    }
                }
                self.rolling = false
            })
            
        }
    }
    func gameIsOver(player player:Int){
        tapLabel.hidden = true
        gameOverMusic.play()
        gameover = true
        gamePlayInProgress = true
        let gameOverLabel = SKLabelNode(fontNamed: "Arial Black")
        if player == 1{
            gameOverLabel.text = "You WON!!!"
        }else{
            gameOverLabel.text = "You LOST!!!"
        }
        gameOverLabel.fontSize = 45
        gameOverLabel.color = SKColor.blueColor()
        gameOverLabel.position =  CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))
        
        self.addChild(gameOverLabel)
        let scaleUp = SKAction.scaleTo(1.2, duration: 1.0)
        let scaleDown = SKAction.scaleTo(1.0, duration: 0.2)
        let sequence = SKAction.sequence([scaleUp,scaleDown])
        gameOverLabel.runAction(sequence) { () -> Void in
            let label = SKLabelNode(fontNamed: "Chalkduster")
            label.position = CGPointMake(CGRectGetMidX(self.frame), 100)
            label.text = "TAP"
            label.fontSize = 20
            label.fontColor = SKColor.greenColor()
            self.addChild(label)
            let fadeOut = SKAction.fadeOutWithDuration(0.3)
            let fadeIn = SKAction.fadeInWithDuration(0.3)
            let sequenceFade = SKAction.sequence([fadeOut,fadeIn])
            let repeatAction = SKAction.repeatActionForever(sequenceFade)
            label.runAction(repeatAction)
        }
        if NSUserDefaults.standardUserDefaults().objectForKey("SnakeGame") == nil{
            NSUserDefaults.standardUserDefaults().setObject(1, forKey: "SnakeGame")
            if player == 1{
                NSUserDefaults.standardUserDefaults().setObject(0, forKey: "SnakeGameComp")
                NSUserDefaults.standardUserDefaults().setObject(1, forKey: "SnakeGameUser")
            }else{
                NSUserDefaults.standardUserDefaults().setObject(1, forKey: "SnakeGameComp")
                NSUserDefaults.standardUserDefaults().setObject(0, forKey: "SnakeGameUser")
            }
            NSUserDefaults.standardUserDefaults().synchronize()
        }else{
            let totalGames:String = NSUserDefaults.standardUserDefaults().valueForKey("SnakeGame") as! String
            var totalgame:Int? = Int(totalGames)
            totalgame = totalgame! + 1
            NSUserDefaults.standardUserDefaults().setValue("\(totalgame)", forKey: "SnakeGame")
            if player == 1{
                
                let userGames:String =  NSUserDefaults.standardUserDefaults().valueForKey("SnakeGameUser") as! String
                var totalWon:Int? = Int(userGames)
                totalWon = totalWon! + 1
                NSUserDefaults.standardUserDefaults().setValue("\(totalWon)", forKey: "SnakeGameUser")
                
            }else{
                let compGames:String =  NSUserDefaults.standardUserDefaults().valueForKey("SnakeGameComp") as! String
                var totalWon:Int? = Int(compGames)
                totalWon = totalWon! + 1
                NSUserDefaults.standardUserDefaults().setValue("\(totalWon)", forKey: "SnakeGameComp")
                
            }
            NSUserDefaults.standardUserDefaults().synchronize()

        }
        
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
       /* Called when a touch begins */
        if gameover{
            restart()
            return
        }
        if gamePlayInProgress{
            if player1Turn  && !rolling && moveFinished {
                moveFinished = false
                rollDice(player: 1)
                tapLabel.hidden = true
            }
        }else{
            startGame()
        }
        
      
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}
