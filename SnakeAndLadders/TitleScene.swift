//
//  TitleScene.swift
//  SnakeAndLadders
//
//  Created by kevin travers on 2/3/16.
//  Copyright © 2016 Bunny Phantom. All rights reserved.
//

import UIKit
import SpriteKit

class TitleScene: SKScene {
    override func didMoveToView(view: SKView) {
        
        //let totalGames: SKLabelNode = childNodeWithName("totalGames") as! SKLabelNode
        //let userGames: SKLabelNode = childNodeWithName("userGames") as! SKLabelNode
        //let computerGames: SKLabelNode = childNodeWithName("computerGames") as! SKLabelNode
       
        
        /*
        totalGames.text = gameRecords().totalGames
        userGames.text = gameRecords().userGames
        computerGames.text = gameRecords().compGames*/
    }
    func gameRecords() -> (totalGames: String, userGames: String, compGames: String) {
      
        var totalGames: String = ""
        var userGames: String = ""
        var compGames: String = ""
        
        
        if NSUserDefaults.standardUserDefaults().objectForKey("SnakeGame") == nil {
            
            totalGames = "0"
            userGames = "0"
            compGames = "0"
            
            
        } else {
            
            totalGames = NSUserDefaults.standardUserDefaults().valueForKey("SnakeGame")! as! String
            userGames = NSUserDefaults.standardUserDefaults().valueForKey("SnakeGameUser")! as! String
            compGames = NSUserDefaults.standardUserDefaults().valueForKey("SnakeGameComp")! as! String
            
        }
        
        return ("Total Games Played : \(totalGames)", "You Won : \(userGames)", "Computer Won : \(compGames)")
        
        
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let gameScene = GameScene(fileNamed: "GameScene")
        let transition = SKTransition.doorwayWithDuration(1.0)
        self.view?.presentScene(gameScene!, transition: transition)
        
    }
}
